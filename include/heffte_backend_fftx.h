/*
    -- heFFTe --
       Univ. of Tennessee, Knoxville
       @date
*/

#ifndef HEFFTE_BACKEND_FFTX_H
#define HEFFTE_BACKEND_FFTX_H

#include "heffte_r2r_executor.h"

#ifdef Heffte_ENABLE_FFTX

#include "fftx3.hpp"
#include "fftx_dftbat_public.h"
#include "fftx_idftbat_public.h"
//#include "fftw3.h"
/*!
 * \ingroup fft3d
 * \addtogroup hefftefftx Backend fftx
 *
 * Wrappers and template specializations related to the FFTX backend.
 * Requires CMake option:
 * \code
 *  -D Heffte_ENABLE_FFTX=ON
 * \endcode
 */

namespace heffte{

namespace backend{
    /*!
     * \ingroup hefftefftx
     * \brief Indicate that the FFTX backend has been enabled.
     */
    template<> struct is_enabled<fftx> : std::true_type{};

}

/*!
 * \ingroup hefftefftx
 * \brief Recognize the FFTX single precision complex type.
 */
//template<> struct is_ccomplex<fftwf_complex> : std::true_type{};
 
/*!
 * \ingroup hefftefftw
 * \brief Recognize the FFTX double precision complex type.
 */
//template<> struct is_zcomplex<fftw_complex> : std::true_type{};

/*!
 * \ingroup hefftefftw
 * \brief Base plan for fftw, using only the specialization for float and double complex.
 *
 * FFTW3 library uses plans for forward and backward fft transforms.
 * The specializations to this struct will wrap around such plans and provide RAII style
 * of memory management and simple constructors that take inputs suitable to HeFFTe.
 */
template<typename, direction> struct plan_fftx{};

/*!
 * \ingroup hefftefftw
 * \brief Plan for the single precision complex transform.
 *
 * \tparam dir indicates a forward or backward transform
 */
template<direction dir>
struct plan_fftx<std::complex<float>, dir>{
    /*!
     * \brief Constructor, takes inputs identical to fftwf_plan_many_dft().
     *
     * \param size is the number of entries in a 1-D transform
     * \param howmanyffts is the number of transforms in the batch
     * \param stride is the distance between entries of the same transform
     * \param dist is the distance between the first entries of consecutive sequences
     */
  //  plan_fftx(int size, int howmanyffts, int stride, int dist);// :
  //        plan(
	     /*
	     fftxf_plan_many_dft(1, &size, howmanyffts, nullptr, nullptr, stride, dist,
                                                    nullptr, nullptr, stride, dist,
                                                    (dir == direction::forward) ? FFTW_FORWARD : FFTW_BACKWARD, FFTW_ESTIMATE
                                )
	     */
  //				)
  //    {}
    /*!
     * \brief Constructor, takes inputs identical to fftwf_plan_many_dft().
     *
     * \param size1 is the number of entries in a 2-D transform, dimension 1
     * \param size2 is the number of entries in a 2-D transform, dimension 2
     * \param howmanyffts is the number of transforms in the batch
     * \param stride is the distance between entries of the same transform
     * \param dist is the distance between the first entries of consecutive sequences
     */
    plan_fftx(int size1, int size2, std::array<int, 2> const &embed, int howmanyffts, int stride, int dist){
        std::array<int, 2> size = {size2, size1};

        if (embed[0] == 0 and embed[1] == 0){
	  /* plan = NULL;fftxf_plan_many_dft(2, size.data(), howmanyffts, nullptr, nullptr, stride, dist,
                                                    nullptr, nullptr, stride, dist,
                                                    (dir == direction::forward) ? FFTW_FORWARD : FFTW_BACKWARD, FFTW_ESTIMATE
						    );*/
        }else{
	  /*plan = NULL;fftxf_plan_many_dft(2, size.data(), howmanyffts, nullptr, embed.data(), stride, dist,
                                                    nullptr, embed.data(), stride, dist,
                                                    (dir == direction::forward) ? FFTW_FORWARD : FFTW_BACKWARD, FFTW_ESTIMATE
						    );*/
        }
    }
    //! \brief Identical to the float-complex specialization.
    plan_fftx(int size1, int size2, int size3){
        std::array<int, 3> size = {size3, size2, size1};
	/* plan = NULL;fftxf_plan_many_dft(3, size.data(), 1, nullptr, nullptr, 1, 1, nullptr, nullptr, 1, 1,
		      (dir == direction::forward) ? FFTW_FORWARD : FFTW_BACKWARD, FFTW_ESTIMATE);*/
    }
    //! \brief Destructor, deletes the plan.
    ~plan_fftx(){ /* fftxf_destroy_plan(plan); */}
    //! \brief Custom conversion to the FFTW3 plan.
    //    operator fftxf_plan() const{ return plan; }
    //! \brief The FFTW3 opaque structure (pointer to struct).
    //    fftwf_plan plan;
};
 
/*!
 * \ingroup hefftefftw
 * \brief Specialization for double complex.
 */
template<direction dir>
struct plan_fftx<std::complex<double>, dir>{
    //! \brief Identical to the float-complex specialization.
  /*
    plan_fftx(int size, int howmanyffts, int stride, int dist) :
        plan(fftx_plan_many_dft(1, &size, howmanyffts, nullptr, nullptr, stride, dist,
                                                   nullptr, nullptr, stride, dist,
                                                   (dir == direction::forward) ? FFTW_FORWARD : FFTW_BACKWARD, FFTW_ESTIMATE
                               ))
        {}
  */
    //! \brief Identical to the float-complex specialization.
    plan_fftx(int size1, int size2, std::array<int, 2> const &embed, int howmanyffts, int stride, int dist){
        std::array<int, 2> size = {size2, size1};

        if (embed[0] == 0 and embed[1] == 0){
	  /*
            plan = fftw_plan_many_dft(2, size.data(), howmanyffts, nullptr, nullptr, stride, dist,
                                                      nullptr, nullptr, stride, dist,
                                                      (dir == direction::forward) ? FFTW_FORWARD : FFTW_BACKWARD, FFTW_ESTIMATE
                                     );
	  */
        }else{
	  /*
            plan = fftw_plan_many_dft(2, size.data(), howmanyffts, nullptr, embed.data(), stride, dist,
                                                      nullptr, embed.data(), stride, dist,
                                                      (dir == direction::forward) ? FFTW_FORWARD : FFTW_BACKWARD, FFTW_ESTIMATE
                                     );
	  */
        }
    }
    //! \brief Identical to the float-complex specialization.
    plan_fftx(int size1, int size2, int size3){
      /*
        std::array<int, 3> size = {size3, size2, size1};
        plan = fftx_plan_many_dft(3, size.data(), 1, nullptr, nullptr, 1, 1, nullptr, nullptr, 1, 1,
                                  (dir == direction::forward) ? FFTW_FORWARD : FFTW_BACKWARD, FFTW_ESTIMATE);
      */
      
    }
    //! \brief Identical to the float-complex specialization.
    ~plan_fftx(){ /* fftx_destroy_plan(plan); */ }
    //! \brief Identical to the float-complex specialization.
    //    operator fftx_plan() const{ return plan; }

    //! \brief Identical to the float-complex specialization.
    //    fftx_plan plan;
    void* plan;
    
};

/*!
 * \ingroup hefftefftx
 * \brief Wrapper around the FFTX API.
 *
 * A single class that manages the plans and executions of fftw3
 * so that a single API is provided for all backends.
 * The executor operates on a box and performs 1-D FFTs
 * for the given dimension.
 * The class silently manages the plans and buffers needed
 * for the different types.
 * All input and output arrays must have size equal to the box.
 */
class fftx_executor{
public:
    //! \brief Constructor, specifies the box and dimension.
    template<typename index>
    fftx_executor(void*, box3d<index> const box, int dimension) :
        size(box.size[dimension]), size2(0),
        howmanyffts(fft1d_get_howmany(box, dimension)),
        stride(fft1d_get_stride(box, dimension)),
        dist((dimension == box.order[0]) ? size : 1),
        blocks((dimension == box.order[1]) ? box.osize(2) : 1),
        block_stride(box.osize(0) * box.osize(1)),
        total_size(box.count()),
        embed({0, 0})
    {}
    //! \brief Merges two FFTs into one.
    template<typename index>
    fftx_executor(void*, box3d<index> const box, int dir1, int dir2) :
        size(box.size[std::min(dir1, dir2)]), size2(box.size[std::max(dir1, dir2)]),
        blocks(1), block_stride(0), total_size(box.count()), embed({0, 0})
    {
        int odir1 = box.find_order(dir1);
        int odir2 = box.find_order(dir2);

        if (std::min(odir1, odir2) == 0 and std::max(odir1, odir2) == 1){
            stride = 1;
            dist = size * size2;
            howmanyffts = box.size[2];
        }else if (std::min(odir1, odir2) == 1 and std::max(odir1, odir2) == 2){
            stride = box.size[0];
            dist = 1;
            howmanyffts = box.size[0];
        }else{ // case of directions (0, 2)
            stride = 1;
            dist = size;
            embed = {static_cast<int>(box.size[2]), static_cast<int>(box.size[1] * box.size[0])};
            howmanyffts = box.size[1];
        }
    }
    //! \brief Merges three FFTs into one.
    template<typename index>
    fftx_executor(void*, box3d<index> const box) :
        size(box.size[0]), size2(box.size[1]), howmanyffts(box.size[2]),
        stride(0), dist(0),
        blocks(1), block_stride(0),
        total_size(box.count()),
        embed({0, 0})
    {}

//! \brief Forward fft, float-complex case.
void forward(std::complex<float> data[]) const{
  //        make_plan(cforward);
  // find the library function for the desired batch/size combination
  /*
  fftx::point_t<2> curr;
  curr.x[0] = 1;			// # batches
  curr.x[1] = 1024;		// size
  transformTuple_t *tupl = fftx_dftbat_Tuple ( curr);
  if ( tupl != NULL ) {
    ( * tupl->initfp )();
    //  init_fftx_dftbat_1_1024_1d();
    for(int i=0; i<blocks; i++){
      
      float* block_data = reinterpret_cast<float*>(data + (i * block_stride) * 2);
      //  fftx_dftbat_1_1024_1d(block_data, block_data);
      ( * tupl->runfp ) ( block_data, block_data );
      // fftwf_execute_dft(*cforward, block_data, block_data);
    }
  }
  else {
    //  Batch = 1, size = 1024 was not in library
			//  Handle error as desired
  }
  */
}
 
   //! \brief Backward fft, float-complex case.
    void backward(std::complex<float> data[]) const{
   /*
     make_plan(cbackward);
     for(int i=0; i<blocks; i++){
     fftwf_complex* block_data = reinterpret_cast<fftwf_complex*>(data + i * block_stride);
     fftwf_execute_dft(*cbackward, block_data, block_data);
     }
   */
    }
    //! \brief Forward fft, double-complex case.
    void forward(std::complex<double> data[]) const{
      //make_plan(zforward);
      // find the library function for the desired batch/size combination
      fftx::point_t<2> curr;
      curr.x[0] = 1;		// # batches
      curr.x[1] = size;		// size
      transformTuple_t *tupl = fftx_dftbat_Tuple ( curr);
      if ( tupl != NULL ) {
	( * tupl->initfp )();
	for(int i=0; i<blocks; i++){
	  double* block_data = reinterpret_cast<double*>(data + (i * block_stride) * 2);
	  for (int j = 0; j < howmanyffts; ++j)
	  //	  fftw_execute_dft(*zforward, block_data, block_data);
	    ( * tupl->runfp ) ( block_data + dist*j*2, block_data + dist*j*2);
	}
      }
      else {
	//  Batch = 1, size = 1024 was not in library
	//  Handle error as desired
      }

      //      std::cout<<"Size: "<<size<<", How many: "<<howmanyffts<<", size2: "<<size2<<std::endl;
      //      std::cout<<"stride: "<<stride<<", block: "<<blocks<<", dist: "<<dist<<std::endl;
      //      std::cout<<"Block Stride:"<<block_stride<<" Ttl Size: "<<total_size<<std::endl;
      //    int size, size2, howmanyffts, stride, dist, blocks, block_stride, total_size;      

    }
    //! \brief Backward fft, double-complex case.
    void backward(std::complex<double> data[]) const{
      fftx::point_t<2> curr;
      curr.x[0] = 1,
      curr.x[1] = size;

      transformTuple_t *tupl = fftx_idftbat_Tuple ( curr );
      if ( tupl != NULL ) {
	( * tupl->initfp )();
	for(int i=0; i<blocks; i++){
	  double* block_data = reinterpret_cast<double*>(data + (i * block_stride) * 2);
	  for (int j = 0; j < howmanyffts; ++j)
	    ( * tupl->runfp ) ( block_data + dist*j*2, block_data + dist*j*2);
	}
      }
      else {
	//  Batch = 1, size = 1024 was not in library
	//  Handle error as desired
      }

      //            std::cout<<"Size: "<<size<<", How many: "<<howmanyffts<<", size2: "<<size2<<std::endl;
      //            std::cout<<"stride: "<<stride<<", block: "<<blocks<<", dist: "<<dist<<std::endl;
      //            std::cout<<"Block Stride:"<<block_stride<<" Ttl Size: "<<total_size<<std::endl;

      
      /*
      make_plan(zbackward);
        for(int i=0; i<blocks; i++){
            fftw_complex* block_data = reinterpret_cast<fftw_complex*>(data + i * block_stride);
            fftw_execute_dft(*zbackward, block_data, block_data);
        }
      */
    }

    //! \brief Converts the deal data to complex and performs float-complex forward transform.
    void forward(float const indata[], std::complex<float> outdata[]) const{
        for(int i=0; i<total_size; i++) outdata[i] = std::complex<float>(indata[i]);
        forward(outdata);
    }
    //! \brief Performs backward float-complex transform and truncates the complex part of the result.
    void backward(std::complex<float> indata[], float outdata[]) const{
        backward(indata);
        for(int i=0; i<total_size; i++) outdata[i] = std::real(indata[i]);
    }
    //! \brief Converts the deal data to complex and performs double-complex forward transform.
    void forward(double const indata[], std::complex<double> outdata[]) const{
        for(int i=0; i<total_size; i++) outdata[i] = std::complex<double>(indata[i]);
        forward(outdata);
    }
    //! \brief Performs backward double-complex transform and truncates the complex part of the result.
    void backward(std::complex<double> indata[], double outdata[]) const{
        backward(indata);
        for(int i=0; i<total_size; i++) outdata[i] = std::real(indata[i]);
    }

    //! \brief Returns the size of the box.
    int box_size() const{ return total_size; }

private:
    //! \brief Helper template to create the plan.
    
    template<typename scalar_type, direction dir>
      void make_plan(void* /*std::unique_ptr<plan_fftw<scalar_type, dir>> &*/ plan) const{
        if (not plan){
	  
            if (dist == 0)
	      plan = NULL;//std::unique_ptr<plan_fftw<scalar_type, dir>>(new plan_fftw<scalar_type, dir>(size, size2, howmanyffts));
            else if (size2 == 0)
	      plan = NULL;//std::ux`nique_ptr<plan_fftw<scalar_type, dir>>(new plan_fftw<scalar_type, dir>(size, howmanyffts, stride, dist));
            else
	      plan = NULL;//std::unique_ptr<plan_fftw<scalar_type, dir>>(new plan_fftw<scalar_type, dir>(size, size2, embed, howmanyffts, stride, dist));
        }
    }
    
    
    int size, size2, howmanyffts, stride, dist, blocks, block_stride, total_size;
    std::array<int, 2> embed;
    /*
    mutable std::unique_ptr<plan_fftw<std::complex<float>, direction::forward>> cforward;
    mutable std::unique_ptr<plan_fftw<std::complex<float>, direction::backward>> cbackward;
    mutable std::unique_ptr<plan_fftw<std::complex<double>, direction::forward>> zforward;
    mutable std::unique_ptr<plan_fftw<std::complex<double>, direction::backward>> zbackward;
    */
    void *cforward;
    void *cbackward;
    void *zforward;
    void *zbackward;    
    
};

/*!
 * \ingroup hefftefftw
 * \brief Specialization for r2c single precision.
 */
//template<direction dir>
//struct plan_fftw<float, dir>{
    /*!
     * \brief Constructor taking into account the different sizes for the real and complex parts.
     *
     * \param size is the number of entries in a 1-D transform
     * \param howmanyffts is the number of transforms in the batch
     * \param stride is the distance between entries of the same transform
     * \param rdist is the distance between the first entries of consecutive sequences in the real sequences
     * \param cdist is the distance between the first entries of consecutive sequences in the complex sequences
     */
  /*
    plan_fftw(int size, int howmanyffts, int stride, int rdist, int cdist) :
        plan((dir == direction::forward) ?	    
	                  fftwf_plan_many_dft_r2c(1, &size, howmanyffts, nullptr, nullptr, stride, rdist,
                                                   nullptr, nullptr, stride, cdist,
                                                   FFTW_ESTIMATE
                                   )
	     :
	                  fftwf_plan_many_dft_c2r(1, &size, howmanyffts, nullptr, nullptr, stride, cdist,
                                                   nullptr, nullptr, stride, rdist,
                                                   FFTW_ESTIMATE
						   )
)
        {}
    //! \brief Identical to the float-complex specialization.
    ~plan_fftw(){ fftwf_destroy_plan(plan); }
    //! \brief Identical to the float-complex specialization.
    operator fftwf_plan() const{ return plan; }
    //! \brief Identical to the float-complex specialization.
    fftwf_plan plan;
};
*/
 
/*!
 * \ingroup hefftefftw
 * \brief Specialization for r2c double precision.
 */
/*
template<direction dir>
struct plan_fftw<double, dir>{
    //! \brief Identical to the float-complex specialization.
    plan_fftw(int size, int howmanyffts, int stride, int rdist, int cdist) :
        plan((dir == direction::forward) ?
             fftw_plan_many_dft_r2c(1, &size, howmanyffts, nullptr, nullptr, stride, rdist,
                                                   nullptr, nullptr, stride, cdist,
                                                   FFTW_ESTIMATE
                                   ) :
             fftw_plan_many_dft_c2r(1, &size, howmanyffts, nullptr, nullptr, stride, cdist,
                                                   nullptr, nullptr, stride, rdist,
                                                   FFTW_ESTIMATE
                                   ))
									   
        {}
    //! \brief Identical to the float-complex specialization.
    ~plan_fftw(){ fftw_destroy_plan(plan);   }
    //! \brief Identical to the float-complex specialization.
    operator fftw_plan() const{ return plan; }
    //! \brief Identical to the float-complex specialization.
    fftw_plan plan;
};
*/
/*!
 * \ingroup hefftefftw
 * \brief Wrapper to fftw3 API for real-to-complex transform with shortening of the data.
 *
 * Serves the same purpose of heffte::fftw_executor but only real input is accepted
 * and only the unique (non-conjugate) coefficients are computed.
 * All real arrays must have size of real_size() and all complex arrays must have size complex_size().
 */

/*
class fftw_executor_r2c{
public:
    *!
     * \brief Constructor defines the box and the dimension of reduction.
     *
     * Note that the result sits in the box returned by box.r2c(dimension).
     *
    template<typename index>
    fftw_executor_r2c(void*, box3d<index> const box, int dimension) :
        size(box.size[dimension]),
        howmanyffts(fft1d_get_howmany(box, dimension)),
        stride(fft1d_get_stride(box, dimension)),
        blocks((dimension == box.order[1]) ? box.osize(2) : 1),
        rdist((dimension == box.order[0]) ? size : 1),
        cdist((dimension == box.order[0]) ? size/2 + 1 : 1),
        rblock_stride(box.osize(0) * box.osize(1)),
        cblock_stride(box.osize(0) * (box.osize(1)/2 + 1)),
        rsize(box.count()),
        csize(box.r2c(dimension).count())
    {}

    //! \brief Forward transform, single precision.
    void forward(float const indata[], std::complex<float> outdata[]) const{
      
        make_plan(sforward);
        for(int i=0; i<blocks; i++){
            float *rdata = const_cast<float*>(indata + i * rblock_stride);
            fftwf_complex* cdata = reinterpret_cast<fftwf_complex*>(outdata + i * cblock_stride);
            fftwf_execute_dft_r2c(*sforward, rdata, cdata);
        }
      
    }
    //! \brief Backward transform, single precision.
    void backward(std::complex<float> const indata[], float outdata[]) const{
      
        make_plan(sbackward);
        for(int i=0; i<blocks; i++){
            fftwf_complex* cdata = const_cast<fftwf_complex*>(reinterpret_cast<fftwf_complex const*>(indata + i * cblock_stride));
            fftwf_execute_dft_c2r(*sbackward, cdata, outdata + i * rblock_stride);
        }
      
    }
    //! \brief Forward transform, double precision.
    void forward(double const indata[], std::complex<double> outdata[]) const{
        
        make_plan(dforward);
        for(int i=0; i<blocks; i++){
            double *rdata = const_cast<double*>(indata + i * rblock_stride);
            fftw_complex* cdata = reinterpret_cast<fftw_complex*>(outdata + i * cblock_stride);
            fftw_execute_dft_r2c(*dforward, rdata, cdata);
        }
	
    }
    //! \brief Backward transform, double precision.
    void backward(std::complex<double> const indata[], double outdata[]) const{
      
      make_plan(dbackward);
        for(int i=0; i<blocks; i++){
            fftw_complex* cdata = const_cast<fftw_complex*>(reinterpret_cast<fftw_complex const*>(indata + i * cblock_stride));
            fftw_execute_dft_c2r(*dbackward, cdata, outdata + i * rblock_stride);
        }
      
    }

    //! \brief Returns the size of the box with real data.
    int real_size() const{ return rsize; }
    //! \brief Returns the size of the box with complex coefficients.
    int complex_size() const{ return csize; }

private:
    //! \brief Helper template to initialize the plan.
    template<typename scalar_type, direction dir>
      void make_plan(std::unique_ptr<plan_fftw<scalar_type, dir>> &  plan) const{
      if (!plan) plan = NULL;//std::unique_ptr<plan_fftw<scalar_type, dir>>(new plan_fftw<scalar_type, dir>(size, howmanyffts, stride, rdist, cdist));
    }

    int size, howmanyffts, stride, blocks;
    int rdist, cdist, rblock_stride, cblock_stride, rsize, csize;
    
    mutable std::unique_ptr<plan_fftw<float, direction::forward>>  sforward;
    mutable std::unique_ptr<plan_fftw<double, direction::forward>> dforward;
    mutable std::unique_ptr<plan_fftw<float, direction::backward>> sbackward;
    mutable std::unique_ptr<plan_fftw<double, direction::backward>> dbackward;
    
};
*/
/*!
 * \ingroup hefftefftw
 * \brief Helper struct that defines the types and creates instances of one-dimensional executors.
 *
 * The struct is specialized for each backend.
 */
template<> struct one_dim_backend<backend::fftx>{
    //! \brief Defines the complex-to-complex executor.
    using executor = fftx_executor;
    //! \brief Defines the real-to-complex executor.
    using executor_r2c = fftx_executor; //void;//fftx_executor_r2c;
};

/*!
 * \ingroup hefftefftw
 * \brief Helper struct that defines the types and creates instances of one-dimensional executors.
 *
 * The struct is specialized for each backend.
 */
template<> struct one_dim_backend<backend::fftx_cos>{
    //! \brief Defines the real-to-real executor.
    using executor = real2real_executor<backend::fftx, cpu_cos_pre_pos_processor, cpu_buffer_factory>;
    //! \brief There is no real-to-complex variant.
    using executor_r2c = void;
};
/*!
 * \ingroup hefftefftw
 * \brief Helper struct that defines the types and creates instances of one-dimensional executors.
 *
 * The struct is specialized for each backend.
 */
template<> struct one_dim_backend<backend::fftx_sin>{
    //! \brief Defines the real-to-real executor.
    using executor = real2real_executor<backend::fftx, cpu_sin_pre_pos_processor, cpu_buffer_factory>;
    //! \brief There is no real-to-complex variant.
    using executor_r2c = void;
};

/*!
 * \ingroup hefftefftw
 * \brief Sets the default options for the fftw backend.
 */
template<> struct default_plan_options<backend::fftx>{
    //! \brief The reshape operations will also reorder the data.
    static const bool use_reorder = true;
};

/*!
 * \ingroup hefftefftw
 * \brief Sets the default options for the fftw backend.
 */
template<> struct default_plan_options<backend::fftx_cos>{
    //! \brief The reshape operations will also reorder the data.
    static const bool use_reorder = true;
};
/*!
 * \ingroup hefftefftw
 * \brief Sets the default options for the fftw backend.
 */
template<> struct default_plan_options<backend::fftx_sin>{
    //! \brief The reshape operations will also reorder the data.
    static const bool use_reorder = true;
};

}

#endif

#endif   /* HEFFTE_BACKEND_FFTW_H */

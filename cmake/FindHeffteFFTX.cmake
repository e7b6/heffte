# - Find the FFTX library
#
# Usage:
#   find_package(FFTX [REQUIRED] [QUIET] )
#
# It sets the following variables:
#   FFTX_FOUND               ... true if fftx is found on the system
#   FFTX_LIBRARIES           ... full path to fftx library
#   FFTX_INCLUDES            ... fftx include directory
#
# The file creates the imported target
#   HEFFTE::FFTX             ... allows liking to the FFTX package
#
# The following variables will be checked by the function
#   FFTX_LIBRARIES          ... fftx libraries to use
#   FFTX_INCLUDES           ... fftx include directory
#   FFTX_HOME               ... if the libraries and includes are not set
#                               and if set, the libraries are exclusively
#                               searched under this path
#

# macro(heffte_find_fftx_libraries)
# # Usage:
# #   heffte_find_fftx_libraries(PREFIX <fftx-root>
# #                              VAR <list-name>
# #                              REQUIRED <list-names, e.g., "fftw3" "fftw3f">
# #                              OPTIONAL <list-names, e.g., "fftw3_threads">)
# #  will append the result from find_library() to the <list-name>
# #  both REQUIRED and OPTIONAL libraries will be searched
# #  if PREFIX is true, then it will be searched exclusively
# #                     otherwise standard paths will be used in the search
# #  if a library listed in REQUIRED is not found, a FATAL_ERROR will be raised
# #
# fftx_imddft_precomp;fftx_imdprdft_precomp;fftx_mddft_precomp;fftx_mdprdft_precomp;fftx_rconv_precomp
#     cmake_parse_arguments(heffte_fftx "" "PREFIX;VAR" "REQUIRED;OPTIONAL" ${ARGN})
#     foreach(heffte_lib ${heffte_fftx_REQUIRED} ${heffte_fftx_OPTIONAL})
#         if (heffte_fftx_PREFIX)
#             find_library(
#                 heffte_fftx_lib
#                 NAMES ${heffte_lib}
#                 PATHS ${heffte_fftx_PREFIX}
#                 PATH_SUFFIXES lib
#                               lib64
#                               ${CMAKE_LIBRARY_ARCHITECTURE}/lib
#                               ${CMAKE_LIBRARY_ARCHITECTURE}/lib64
#                               lib/${CMAKE_LIBRARY_ARCHITECTURE}
#                               lib64/${CMAKE_LIBRARY_ARCHITECTURE}
#                 NO_DEFAULT_PATH
#                         )
#         else()
#             find_library(
#                 heffte_fftw_lib
#                 NAMES ${heffte_lib}
#                         )
#         endif()
#         if (heffte_fftw_lib)
#             list(APPEND ${heffte_fftx_VAR} ${heffte_fftx_lib})
#         elseif (${heffte_lib} IN_LIST "${heffte_fftx_REQUIRED}")
#             message(FATAL_ERROR "Could not find required fftx component: ${heffte_lib}")
#         endif()
#         unset(heffte_fftx_lib CACHE)
#     endforeach()
#     unset(heffte_lib)
# endmacro(heffte_find_fftx_libraries)

# if user has not provided FFTX_HOME, then check with the environment
if ( DEFINED ENV{FFTX_HOME} AND NOT FFTX_HOME )
    set ( FFTX_HOME "$ENV{FFTX_HOME}" )
    message ( STATUS "Set FFTX_HOME = $ENV{FFTX_HOME}" )
endif ()

##  Include the FFTX CMake functions...
include ( "${FFTX_HOME}/CMakeIncludes/FFTXCmakeFunctions.cmake" )

FFTX_find_libraries ()
message ( STATUS "Include paths:   ${FFTX_LIB_INCLUDE_PATHS}" )
message ( STATUS "Libraries found: ${FFTX_LIB_NAMES}" )
message ( STATUS "Library path is: ${FFTX_LIB_LIBRARY_PATH}" )

# respect user provided FFTX_LIBRARIES
if ( NOT FFTX_LIBRARIES )
    set ( FFTX_LIBRARIES ${FFTX_LIB_NAMES} )
    # heffte_find_fftx_libraries(
    #     PREFIX ${FFTX_HOME}
    #     VAR FFTX_LIBRARIES
    #     REQUIRED "fftx_mddft_precomp"
    #     OPTIONAL "fftx_threads" "fftx_threads" "fftx_omp" "fftx_omp"
    #                            )
    # if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
    #     find_package(OpenMP REQUIRED)
    #     list(APPEND FFTX_LIBRARIES ${OpenMP_CXX_LIBRARIES})
    # else()
    #     if ("fftx_omp" IN_LIST FFTX_LIBRARIES)
    #         list(APPEND FFTX_LIBRARIES "-lgomp")
    #     endif()
    # endif()
endif ()

# respect user provided FFTX_INCLUDES
if ( NOT FFTX_INCLUDES )
    set ( FFTX_INCLUDES ${FFTX_LIB_INCLUDE_PATHS} )
    # if (FFTX_HOME)
    #     find_path(
    #         FFTX_INCLUDES
    #         NAMES "fftx3.hpp" 
    #         PATHS ${FFTX_HOME} 
    #         PATH_SUFFIXES "include" "examples/library"
    #         NO_DEFAULT_PATH
    #              )
    # else()
    #     find_path(
    #         FFTX_INCLUDES
    #         NAMES "fftx3.hpp"
    #              )
    # endif()
endif ()

# handle components and standard CMake arguments
include ( FindPackageHandleStandardArgs )
find_package_handle_standard_args ( HeffteFFTX DEFAULT_MSG
				    FFTX_INCLUDES FFTX_LIBRARIES )
		      
message ( STATUS "Package HeffteFFTX found = ${HeffteFFTX_FOUND}" )
message ( STATUS "FFTX_INCLUDES = ${FFTX_INCLUDES}" )
message ( STATUS "FFTX_LIBRARIES = ${FFTX_LIBRARIES}" )

##  CUDA is required -- turn it on in the top level CMakeLists
# find_package(CUDA REQUIRED)
# list(APPEND CUDA_NVCC_FLAGS "-std=c++11")
# cuda_add_cufft_to_target(Heffte)
# target_include_directories(Heffte PUBLIC $<INSTALL_INTERFACE:${CUDA_INCLUDE_DIRS}>)
				
# create imported target
add_library                   ( Heffte::FFTX INTERFACE IMPORTED GLOBAL )
target_link_libraries         ( Heffte::FFTX INTERFACE ${FFTX_LIBRARIES} )
target_link_directories       ( Heffte::FFTX INTERFACE ${FFTX_LIB_LIBRARY_PATH} )
##  set_target_properties         ( Heffte::FFTX PROPERTIES INTERFACE_INCLUDE_DIRECTORIES ${FFTX_INCLUDES} )
target_include_directories    ( Heffte::FFTX INTERFACE ${FFTX_INCLUDES} )
##  target_include_directories    ( Heffte PUBLIC $<INSTALL_INTERFACE:${FFTX_INCLUDES}> )
